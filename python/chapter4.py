#!/usr/bin/env python

import turtle
import math

bob = turtle.Turtle()
print(bob)

def square(t):
      for i in range(4):
          t.fd(90)
          t.lt(90)

square(bob)

def square(t, length):
    for i in range(4):
        t.fd(length)
        t.lt(90)

square(bob, 100)

def polygon(t, n, length):
    angle = 360 / n
    for i in range(n):
        t.fd(length)
        t.lt(angle)

polygon(bob, 7, 100)

def arc(t, r, angle):
    arc_length = 2 * math.pi * r * angle / 360
    n = int (arc_length / 3) + 1
    step_length = arc_length / n
    step_angle = float(angle) / n
    polyline(t,n, step_length, step_angle)

    def circle(t, r):
        arc(t, r, 360)
