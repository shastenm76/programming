#!/usr/bin/python

"""This is going to read a file unsorted_fruits.txt and it is going to save it in another file sorted_fruits.txt in alphabetical order"""

# This will pull up the file and read each line, then close
f = open("/home/shasten/unsorted_fruits.txt", "r")
fruit = f.readlines()
f.close()

#This will sort the items in the list
fruit.sort()

#This will prepare a file, write the sorted items in new file, and then close.
s= open("/home/shasten/sorted_fruits.txt", "w")
for f in fruit:
    s.write(f)
s.close()

