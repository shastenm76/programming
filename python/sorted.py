#!/usr/bin/python
import pyperclip

infile = open("/home/shasten/unsorted_fruits.txt", "r")
outfile = open("/home/shasten/sorted_fruits", "w")

fruit=infile.read(50)
#outfile.write(fruit)

with open('/home/shasten/unsorted_fruits.txt', 'r') as f:
    words = f.readlines()

      #Strip the words of the newline characters (you may or may not want to do this):
    words = [word.strip() for word in words]

    #sort the list:
    words.sort()
    print(words)

pyperclip.copy(words)
