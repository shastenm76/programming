#!/usr/bin/python
#chapter 7
def main():

	print("What's the definition ")
	print("reassignment")
	print("update")
	print('initialization')
	print('increment')
	print('decrement')
	print('iteration')
	print('infinite-loop')
	print('algorithm')

	choice = input ("Definition of: ")

	if choice == "reassignment":
		print("Assigning a new value to a variable that already exists")
	elif choice == "update":
		print("An assignment where the new value of the variable depends on the old")
	elif choice == "initialization":
		print("An assignment that gives an initial value to a variable that will be updated")
	elif choice == "increment":
		print("An update that increases the values of a variable (often by one")
	elif choice == "decrement":
		print("And update that decreases the value of a variable")
	elif choice == "iteration":
		print("Repeated execution of a set of statements using either a recursive function call or a loop")
	elif choice == "infinity-loop":
		print("A loop in which the terminating condition is never satisfied")
	elif choice == "algorithm":
		print("A general process for solving a category of problems")
main()
	
