#!/usr/bin/env python
def do_four(f):
    f()
    f()
    f()
    f()

def print_vert():
    print('|' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '|' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '|')


print ('+','-','-','-','-','+','-','-','-','-','+')
do_four(print_vert)
print ('+','-','-','-','-','+','-','-','-','-','+')
do_four(print_vert)
print ('+','-','-','-','-','+','-','-','-','-','+')
