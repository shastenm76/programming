#!/usr/bin/env python

import turtle
import math

bob = turtle.Turtle()
print(bob)

#def shape(t):
#    for i in range(6):
#        t.fd(60)
#        t.lt(60)
#        t.lt(50)
#        t.fd(50)
#        t.rt(50)
#        t.rt(50)
#        t.lt(50)
#
#shape(bob)

def shape(t):
    for i in range(4):
        t.bk(60)
        t.lt(90)
        t.bk(60)
        t.lt(90)
        t.bk(60)
        t.rt(90)

shape(bob)
