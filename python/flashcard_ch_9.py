#!/usr/bin/python
#chapter 9

def main():

	print("What's the definition of a/an")
	print("file-object")
	print("reduction to a previously solved problem")
	print("special_case")

	choice = input ("Definition of ")

	if choice == "file-object":
		print("A value that represents an open file.")
	elif choice == "reduction":
		print("A way of solving a problem by expressing it as an instance of a previously solved problem.")
	elif choice == "special-case":
		print(" A test case that is atypical or non-obvious (and less likely to be handled correctly.")

main()