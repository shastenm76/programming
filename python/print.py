#!/usr/bin/env python

print(1, 2, 3)                      # 123
print(4, 5, 6)                      # 456
print(1, 2, 3, sep=', ', end='. ')  #1,2,3.
print(4, 5, 6, sep=', ', end='. ')  #1,2,3.4,5,6.
print()
print(1, 2, 3, sep='', end=' -- ')  #1,2,3 --
print(4, 5, 6, sep=' * ', end='.')   #1,2,3 -- 4,5,6.
