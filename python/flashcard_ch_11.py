#!/usr/bin/python
#chapter 11

#!/usr/bin/python
#chapter 8
def main():

	print("What's the definition ")
	print("mapping")
	print("dictionary")
	print('key_value pair')
	print('item')
	print('key')
	print('value')
	print('implementation')
	print('hashtable')
	print('hash function')
	print('hashable')
	print('lookup')
	print('reverse lookup')
	print('raise statement')
	print('singleton')
	print('call graph')
	print('memo')
	print('global variable')
	print('global statement')
	print('flag')
	print('declaration')

	choice = input ("Definition of ")

	if choice == "mapping":
		print("A relationship in which each element of one set corresponds to an element of another set.")
	elif choice == "dictionary":
		print("A mapping from keys to their corresponding values.")
	elif choice == "key-value pair":
		print("The representation of the mapping from a key to a value.")
	elif choice == "item":
		print("In a dictionary, another name for a key-value pair.")
	elif choice == "key":
		print("An object that appears in a dictionary as the first part of a key-value pair.")
	elif choice == "value":
		print("An object that appears in a dictionary as the second part of a key-value pair.")
	elif choice == "implementation":
		print("A way of performing a computation.")
	elif choice == "lookup":
		print("A dictionary operation that a key and finds the corresponding value.")
	elif choice == "reverse lookup":
		print("A dictionary operation that takes a value and finds one or more keys that map to it.")
	elif choice == "raise statement":
		print("A statement that (deliberately) raises an exception.")
	elif choice == "singleton":
		print("A list (or other sequence) with a single element.")
	elif choice == "call graph":
		print("A diagram that shows every frome created during the execution of a program, with an arrow from each caller to each callee.")
	elif choice == "memo":
		print("A computed value  stored to avoid unnecessary future computation.")
	elif choice == "global variable":
		print("A variable defined outside a function.  Global variables can be accessed from any function.")
	elif choice == "global statement":
		print("A statement that declares a variable name global.")
	elif choice == "flag":
		print("A boolean variable used to indicate whether a condition is true.")
	elif choice == "declaration":
		print("A statement like global that tells the interpreter something about a variable.")
	
main()
