#!/usr/bin/python
#chapter 10

#!/usr/bin/python
#chapter 8
def main():

	print("What's the definition ")
	print("list")
	print("nested list")
	print('accumulator')
	print('augmented assignment')
	print('reduce')
	print('map')
	print('filter')
	print('object')
	print('equivalent')
	print('identical')
	print('reference')
	print('aliasing')
	print('delimiter')
	choice = input ("Definition of ")

	if choice == "list":
		print("A sequence of values")
	elif choice == "nested list":
		print("One of the values in a list (or other sequence), also called items.")
	elif choice == "accumulator":
		print("A variable used in a loop to add up or accumulate a result.")
	elif choice == "augmented assignment":
		print("A statement that updates the value of a variable using an operator like +=")
	elif choice == "reduce":
		print("A processing patter that traverses a sequence and accumulates the elements into a single result.")
	elif choice == "map":
		print("A processing pattern that traverses a sequence and performs an operation on each element.")
	elif choice == "filter":
		print("A processing patter that traverses a list and selects the elements that satisfy some criterion.")
	elif choice == "object":
		print("Somethintg a variable can refer to.  An object is a type of value.")
	elif choice == "equivalent":
		print("Having the same value.")
	elif choice == "identical":
		print("The association between a variable and its value.")
	elif choice == "reference":
		print("A circumstance where two or more variables refer to the same object.")
	elif choice == "aliasing":
		print("A character or string used to indicate where a string should be split.")
	elif choice == "delimiter":
		print()
main()
