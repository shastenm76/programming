#!/usr/bin/python
#chapter 8
def main():

	print("What's the definition ")
	print("object")
	print("sequence")
	print('item')
	print('index')
	print('slice')
	print('empty-string')
	print('immutable')
	print('traverse')
	print('search')
	print('counter')
	print('invocation')
	print('optional-argument')

	choice = input ("Definition of ")

	if choice == "object":
		print("Something a variable can refer to.")
	elif choice == "sequence":
		print("An ordered collection of values where each value is identified by an integer index")
	elif choice == "item":
		print("One of the values in a sequence")
	elif choice == "index":
		print("An integer value used to select an item in a sequence, such as a character in a string.  Indices start from 0.")
	elif choice == "slice":
		print("A part of a string specified by a range of indices.")
	elif choice == "empty-string":
		print("A string with no characters and length 0, represented by two quotation marks")
	elif choice == "immutable":
		print("The property of a sequence whose items cannot be changed.")
	elif choice == "traverse":
		print("To iterate through the items in a sequence, performing a similar operation on each.")
	elif choice == "search":
		print("A pattern of traversal that stops when it finds what it is looking for.")
	elif choice == "counter":
		print("A variable used to count something, usually initialized to zero and then incremented.")
	elif choice == "invocation":
		print("A statement that calls a method.")
	elif choice == "optional-argument":
		print("A function or method argument that is not required")

main()
