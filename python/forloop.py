#!/usr/bin/env python

result = 0
n = 5
for i in range(1, n + 1):   # n + 1 is another way to say 6
    result += i             # remember that range will count one less than the indicated parameter.
    # this ^^ is the shorthand for
    # result = result + i
print(result)

#Another way to write the same thing.

result = 0
for i in range(1, 6):
    result += i
    # this ^^ is the shorthand for
    # result = result + i
print(result)

#  the for loop basically counts recursively
#  giving the variable i the value of 1-5 (6).
#  The result is the addition of i recursively 
#  until the code ends when i is 5 (6).

# When i is 0, the $result is 0
# When i is 1, the $result is 1
# When i is 2, the $result is 3
# When i is 3, the $result is 6
# When i is 4, the $result is 10
# When i is 5, the $result is 15
